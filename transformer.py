import sys
import pandas as pd
from rdflib import Graph, Literal, URIRef, Namespace
from rdflib.namespace import RDF, RDFS, XSD

def generate_graph(csv_path: str, all_data: bool = True):
    # Load CSV data
    household_data = pd.read_csv(csv_path,sep=",")
    docs = pd.read_csv("docs.csv",sep=";")

    # Create an empty Graph
    g = Graph()
    saref = Namespace("https://saref.etsi.org/core/")

    # Create an RDF URI node to use as the subject for multiple triples
    industrial = URIRef("http://example.org/IndustrialBuilding")
    residential = URIRef("http://example.org/ResidentialBuilding")
    public = URIRef("http://example.org/PublicBuilding")
    hourly_active_power = URIRef("http://example.org/HourlyActivePower")
    metering_function = URIRef("http://example.org/MeteringFunction")

    # Make industrial subtype of building
    g.add((industrial, RDFS.subClassOf, URIRef("http://example.org/Building")))
    g.add((residential, RDFS.subClassOf, URIRef("http://example.org/Building")))
    g.add((public, RDFS.subClassOf, URIRef("http://example.org/Building")))
    g.add((metering_function, saref.hasMeterReadingType, hourly_active_power))


    # Creating multiple buidlings
    for col_count, raw_col_name in enumerate(household_data.columns, start=1):
        # Print progress
        print(f"Processing column {col_count}/{len(household_data.columns)}")

        if "DE_KN_" not in raw_col_name:
            continue
        
        col_name = raw_col_name.replace("DE_KN_","")
        col_name_split = col_name.split("_")

        building = URIRef(f"http://example.org/{col_name_split[0]}")

        if "industrial" in col_name_split[0]:
            g.add((building, RDF.type, industrial))
        elif "residential" in col_name_split[0]:
            g.add((building, RDF.type, residential))
        elif "public" in col_name_split[0]:
            g.add((building, RDF.type, public))
        else:
            print(f"Unknown building type {col_name_split[0]}")
        
        # Add meter to buidling
        meter = URIRef(f"http://example.org/{col_name}")
        
        g.add((meter, RDF.type, saref.meter))
        g.add((meter, saref.measuresProperty, hourly_active_power))
        g.add((meter, saref.hasFunction, metering_function))
        g.add((meter, RDFS.label, Literal(col_name.replace("_", " "), lang="en")))
        g.add((meter, URIRef(f"http://example.org/locatedIn"), building))

        # Find comment in docs provided by household data (copied to docs.csv)
        label = docs[docs["Field Name"] == raw_col_name].values[0][2]
        g.add((meter, RDFS.comment, Literal(label, lang="en")))

        # Add measurements to the meter, therefore loop over the rows of the current column
        i = 0
        for index, row in household_data.iterrows():
            time = row["utc_timestamp"]
            value = row[raw_col_name]
            if pd.isnull(value):
                continue
            
            measurement = URIRef(f"http://example.org/{col_name}_{i}")
            g.add((measurement, RDF.type, saref.Measurement))
            g.add((measurement, saref.hasValue, Literal(value, datatype=XSD.decimal)))
            g.add((measurement, saref.hasTimestamp, Literal(time, datatype=XSD.dateTime)))
            g.add((measurement, saref.isMeasuredIn, Literal("kW")))
            g.add((measurement, saref.relatesToProperty, hourly_active_power))

            g.add((meter, saref.makesMeasurement, measurement))
            g.add((metering_function, saref.hasMeterReading, measurement))
            
            
            i += 1
            if not all_data and i > 3:
                break
            
    # Save graph as Turtle file
    g.serialize(destination='graph.ttl', format='turtle')


# Run main function with given system arguments
if len(sys.argv) > 2:
    if sys.argv[2] == "0" or sys.argv[2] == "False" or sys.argv[2] == "false":
        generate_graph(sys.argv[1], False)
    else:
        generate_graph(sys.argv[1], True)
elif len(sys.argv) > 1:
    generate_graph(sys.argv[1])
else:
    raise ValueError("No CSV path provided, please provide a path to the CSV file as an argument")

