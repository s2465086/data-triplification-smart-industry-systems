### Data triplification - Smart Industry Systems

## Description
A Python script that transforms information from a CSV file to a RDF graph in Turtle

## Installation
1. Clone the repository:

2. Install the required dependencies (pip install -r requirements.txt)

## Usage
Use python to run the file transformer.py arg0 arg1, where the args mean the following:
- arg0: required, path to the input CSV
- arg1: optional, do you want to include all the non empty data from the input CSV? If so, the process can take up to 10 minutes, due the large amount of data. If this argument is False, only 3 measurements from each meter is used, if left empty of given True, all data from the CSV is used.